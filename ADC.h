/* 
 * File:   ADC.h
 * Author: User
 *
 * Created on April 6, 2014, 8:44 PM
 */

#ifndef ADC_H
#define	ADC_H

#ifdef	__cplusplus
extern "C" {
#endif
void initFVR(char gain){
        FVRCONbits.ADFVR=gain;
        FVRCONbits.FVREN=1;
        while(FVRCONbits.FVRRDY!=1);
}
    void initADC() {        
        ADCON1bits.ADCS = 6;     //clock for 32 MHz
        ADCON1bits.ADPREF=3;     //0 is VDD/ 3 is internal FVR
        ADCON0bits.CHS = 3;      //select voltage read pin
        ADCON1bits.ADFM = 1;     // right justified
        ADIF = 0;
        ADIE = 1;
        INTCONbits.PEIE=1;
        ADCON0bits.ADON = 1;
    }

    void disableADC() {
        ADCON0bits.ADON = 0;
    }

   float readADC() {
        return (((ADRESH&0xFF) << 8) + ADRESL);
    }

    void requestADC() {
        //if (!ADCON0bits.GO_nDONE)
            ADCON0bits.GO_nDONE = 1;
        }
    


#ifdef	__cplusplus
}
#endif

#endif	/* ADC_H */

