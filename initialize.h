/* 
 * File:   initialize.h
 * Author: User
 *
 * Created on April 6, 2014, 8:46 PM
 */
#include <xc.h>
#ifndef INITIALIZE_H
#define	INITIALIZE_H

#ifdef	__cplusplus
extern "C" {
#endif
    void initTimer4(){


    }
    void initPins() {
        _delay(5000);
        TRISA = 0X12; //Set pin I/O direction Port A
        ANSELA = 0x10;        
        TRISC = 0x38; //Set pin I/O direction Port C
        LATCbits.LATC0=1;
        LATCbits.LATC1=1;
        LATCbits.LATC2=0;
        LATAbits.LATA2=0;

        ANSELC = 0;        
        ENABLE_BOOST = 1;
        OPTION_REGbits.nWPUEN=0;       
        LED_PIN=1;
        prevState = FLOAT; //RECORD PREVIOUS STATE
        currentState = FLOAT; //RECORD CURRENT STATE
        ENABLE_PULL = 1;
        IO1_PULL = 1; //pull ups
        IO2_PULL = 1;   //pull ups
    }
    void initClk(){
        OSCCONbits.SCS=0;
        OSCCONbits.IRCF=0xE;    //set osc freq to 8/32Mhz
        OSCCONbits.SPLLEN=1;    //Enable PLL if not configuration words
    }
    void initWatchdog(){
        WDTCONbits.WDTPS=0x0F;  //watchdog timer (A=1sec   B=2sec  C=4sec  D=8sec  F=16sec)
    }
    void systemInit(){
    //Clock configuration
    initClk(); 
    //pin configuration
    initPins();
    __delay_ms(2000);
    //watchdog enabled and config
    initWatchdog();
    //Fixed voltage reference init
    initFVR(2);
    //ADC init
    initADC();
    //UART init
    initUART();
    //Timer0 allows for automation of the ADC read system
    //initializeTimer0();
    //interruptTimer0();
    //Timer 1 allows for LED blink of life
    initializeTimer1();
    interruptTimer1();
//    initializeTimer2();
//    interruptTimer2();
    //Interrupt pin allows for automated input handling
    interruptPin();
    //Overhead interrupt service init
    interruptServiceInit();

    WDTreset();

    }
    

#ifdef	__cplusplus
}
#endif

#endif	/* INITIALIZE_H */

