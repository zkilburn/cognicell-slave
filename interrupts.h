/* 
 * File:   interrupts.h
 * Author: User
 *
 * Created on April 6, 2014, 8:45 PM
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H

#ifdef	__cplusplus
extern "C" {
#endif

    void interruptPin() {
        PPSLOCKED = 0;
        RA1PPS = 20;    //UART configuration A1
        INTPPS = 0x13;
        PPSLOCKED = 1;
        INTF = 0;
        INTE = 1;
    }

    void initializeTimer0() {
        OPTION_REGbits.nWPUEN = 0; //Pull-ups (0-on/1-off)
        OPTION_REGbits.INTEDG = 0; //interrupt falling (inverted logic)
        OPTION_REGbits.PSA = 0; //Prescaler Enable Bit
        OPTION_REGbits.PS = 0; // 1:2 (000) -> 1:256 (111)
    }

    void interruptTimer0() {

        OPTION_REGbits.TMR0CS = 0; //Set the Timer to internal clock
        TMR0IF = 0;
        TMR0IE = 1;

    }

    void initializeTimer1() {
        T1CONbits.T1CKPS = 3; //T1 prescaler == (3=[1:8] ; 2 = [1:4] ; 1 = [1,2]  ; 0 = [1,1])
        T1CONbits.TMR1CS = 3; //timer clock select FOSC
        TMR1H = 0xF0;
        TMR1L = 0;
    }

    void interruptTimer1() {
        PIR1bits.TMR1IF = 0;
        PIE1bits.TMR1IE = 1;
        T1CONbits.TMR1ON = 1;
    }

    void initializeTimer2() {
        CCP1CONbits.CCP1M = 0xF;
        CCPR1L = 255;
        T2CONbits.T2CKPS = 3; //T1 prescaler == (3=[1:8] ; 2 = [1:4] ; 1 = [1,2]  ; 0 = [1,1])
    }

    void interruptTimer2() {
        PIR1bits.TMR2IF = 0;
        PIE1bits.TMR2IE = 1;
        T2CONbits.TMR2ON = 1;
    }

    void interruptServiceInit() {
        PEIE = 1;
        GIE = 1;
    }

    void interrupt isr(void) {
        static int increment = 0, increment2 = 0;
        static bool speakKeeper = false;
        
        //If it was the timer0 interrupt hit
        if (INTF) {
            checkInputs();
            INTF = 0;
        } else if (PIR1bits.TMR1IF) {
            if (increment > 2) {
                if (spoke && speakKeeper) {
                    speakKeeper=false;
                    spoke=false;
                }else if (spoke) {
                        speakKeeper = true;
                    }

                requestADC();
                increment = 0;
            } else increment++;

            if (increment2 > 5) {
                weAreAlive = true;
                increment2 = 0;
            } else increment2++;

            //reset timer to determined value
            TMR1H = 0xFF;
            TMR1L = 0;

            PIR1bits.TMR1IF = 0;
        } else if (TMR0IF) {

            TMR0IF = 0;
        } else if (ADIF) {
            voltageRead = readADC();
            voltageRead = ((voltageRead * (4400 / 512)) - 600);
//            if(((unsigned char)voltageRead>100)&&((unsigned char)voltageRead<255)){
//                volts=(unsigned char) voltageRead;
//            }
            volts=(unsigned int)voltageRead;
            ADIF = 0;
        }
        else if (PIR1bits.TMR2IF) {
            PIR1bits.TMR2IF = 0;
        } else {
        }

    }


#ifdef	__cplusplus
}
#endif

#endif	/* INTERRUPTS_H */

