/* 
 * File:   states.h
 * Author: User
 *
 * Created on April 6, 2014, 8:49 PM
 */

#ifndef STATES_H
#define	STATES_H

#ifdef	__cplusplus
extern "C" {
#endif

#define transPeriod 75

    void updateState() {

        if (prevState != currentState) {
//            PEIE=0;
//            GIE=0;
            switch (currentState) {
                case POSITIVE:
                    setPositive(prevState);
                    break;
                case NEGATIVE:
                    setNegative(prevState);
                    break;
                case BYPASS:
                    setBypass(prevState);
                    break;
                case FLOAT:
                    setFloat(prevState);
                    break;
                default:
                    currentState = FLOAT;
                    break;
            }
            prevState = currentState;
            //make our pulses with not milivolts but decivolts :)
            //                                     -implied hundredths
            //                      -Delay about 100 us (800 cycles)
            
//            int tt=0;
//            int stateOfOut=IO_OUT;
//            while(tt<50) { //for(i=0;i<(volts/10);i++){
//
//
//                tt++;
//
//                if(stateOfOut==0){
//                    stateOfOut=1;
//                    _delay(2500);
//
//                }
//                else{
//                    stateOfOut=0;
//                }
//                 IO_OUT = stateOfOut;
//                _delay(2500);
//            }
//
//        PEIE=1;
//        GIE=1;
        }

    }

    void setNegative(enum State oldState) {
        switch (oldState) {
            case POSITIVE:
                R_UP = P_OFF;
                L_DN = N_OFF;
                _delay(transPeriod);
                L_UP = P_ON;
                R_DN = N_ON;
                break;
            case FLOAT:
                R_UP = P_OFF;
                L_DN = N_OFF;
                _delay(transPeriod);
                L_UP = P_ON;
                R_DN = N_ON;
                break;
            case BYPASS:
                R_UP = P_OFF;
                L_DN = N_OFF;
                _delay(transPeriod);
                L_UP = P_ON;
                R_DN = N_ON;
                break;
            default:
                R_UP = P_OFF;
                L_DN = N_OFF;
                _delay(transPeriod);
                L_UP = P_ON;
                R_DN = N_ON;
                break;
        }
    }

    void setPositive(enum State oldState) {
        switch (oldState) {
            case NEGATIVE:
                L_UP = P_OFF;
                R_DN = N_OFF;
                _delay(transPeriod);
                R_UP = P_ON;
                L_DN = N_ON;
                break;
            case FLOAT:
                L_UP = P_OFF;
                R_DN = N_OFF;
                _delay(transPeriod);
                R_UP = P_ON;
                L_DN = N_ON;
                break;
            case BYPASS:
                L_UP = P_OFF;
                R_DN = N_OFF;
                _delay(transPeriod);
                R_UP = P_ON;
                L_DN = N_ON;
                break;
            default:
                L_UP = P_OFF;
                R_DN = N_OFF;
                _delay(transPeriod);
                R_UP = P_ON;
                L_DN = N_ON;
                break;
        }
    }

    void setBypass(enum State oldState) {
        switch (oldState) {
            case NEGATIVE:
                L_UP = P_ON;
                L_DN = N_OFF;

                _delay(transPeriod);
                R_UP = P_ON;
                R_DN = N_OFF;
                break;
            case POSITIVE:
                L_UP = P_ON;
                L_DN = N_OFF;

                _delay(transPeriod);
                R_UP = P_ON;
                R_DN = N_OFF;
                break;
            case FLOAT:
                L_UP = P_ON;
                L_DN = N_OFF;

                _delay(transPeriod);
                R_UP = P_ON;
                R_DN = N_OFF;
                break;
            default:
                L_UP = P_ON;
                L_DN = N_OFF;

                _delay(transPeriod);
                R_UP = P_ON;
                R_DN = N_OFF;
                break;
        }
    }

    void setFloat(enum State oldState) {
        switch (oldState) {
            case NEGATIVE:
                R_DN = N_OFF;
                L_UP = P_OFF;
                break;
            case POSITIVE:
                L_DN = N_OFF;
                R_UP = P_OFF;
                break;
            case BYPASS:
                L_UP = P_OFF;
                R_UP = P_OFF;
                break;
            default:
                L_UP = P_OFF;
                L_DN = N_OFF;

                _delay(transPeriod);
                R_UP = P_OFF;
                R_DN = N_OFF;
                break;
        }
    }


#ifdef	__cplusplus
}
#endif

#endif	/* STATES_H */

